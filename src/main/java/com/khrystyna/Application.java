package com.khrystyna;

import com.khrystyna.views.ShipClassView;
import com.khrystyna.views.View;

public class Application {
    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        View view = new ShipClassView();
        view.print();
    }
}
