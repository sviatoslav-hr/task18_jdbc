package com.khrystyna.annotations;

public enum DateType {
    DATE, TIME, DATETIME
}
