package com.khrystyna.dao;

import com.khrystyna.dao.GeneralDao;
import com.khrystyna.models.ShipClass;

public interface ShipClassDao extends GeneralDao<ShipClass, Integer> {
}
