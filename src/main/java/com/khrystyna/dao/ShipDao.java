package com.khrystyna.dao;

import com.khrystyna.models.Ship;

public interface ShipDao extends GeneralDao<Ship, Integer> {
}
