package com.khrystyna.dao.impl;

import com.khrystyna.dao.ShipClassDao;
import com.khrystyna.models.ShipClass;
import com.khrystyna.services.ConnectionManager;
import com.khrystyna.reflection.Transformer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class ShipClassDaoImpl implements ShipClassDao {
    private static Transformer<ShipClass> transformer = new Transformer<>(ShipClass.class);
    private static final String FIND_ALL = "SELECT * FROM " + transformer.getTableName();

    @Override
    public List<ShipClass> findAll() throws SQLException {
        List<ShipClass> classes = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(FIND_ALL);
        while (resultSet.next()) {
            classes.add(transformer.transform(resultSet));
        }
        return classes;
    }

    @Override
    public ShipClass findById(Integer integer) throws SQLException {
        return null;
    }

    @Override
    public int create(ShipClass entity) throws SQLException {
        return 0;
    }

    @Override
    public int update(ShipClass entity) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        return 0;
    }
}
