package com.khrystyna.dao.impl;

import com.khrystyna.dao.ShipDao;
import com.khrystyna.models.Ship;
import com.khrystyna.services.ConnectionManager;
import com.khrystyna.reflection.Transformer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class ShipDaoImpl implements ShipDao {
    private static Transformer<Ship> transformer = new Transformer<>(Ship.class);
    private static final String FIND_ALL = "SELECT * FROM " + transformer.getTableName();

    @Override
    public List<Ship> findAll() throws SQLException {
        List<Ship> ships = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(FIND_ALL);
        while (resultSet.next()) {
            ships.add(transformer.transform(resultSet));
        }
        return ships;
    }

    @Override
    public Ship findById(Integer integer) throws SQLException {
        return null;
    }

    @Override
    public int create(Ship entity) throws SQLException {
        return 0;
    }

    @Override
    public int update(Ship entity) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        return 0;
    }
}
