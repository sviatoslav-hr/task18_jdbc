package com.khrystyna.models;

import com.khrystyna.annotations.*;

import java.time.LocalDateTime;

@Entity
public class Battle {
    @PrimaryKey
    private Integer id;
    private String name;
    @Date(type = DateType.DATETIME)
    private LocalDateTime date;

}
