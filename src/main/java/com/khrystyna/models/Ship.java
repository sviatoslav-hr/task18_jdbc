package com.khrystyna.models;

import com.khrystyna.annotations.Column;
import com.khrystyna.annotations.Entity;
import com.khrystyna.annotations.ForeignKey;
import com.khrystyna.annotations.PrimaryKey;
import lombok.Data;

@Entity
@Data
public class Ship {
    @PrimaryKey
    private Integer id;
    private String name;
    @Column(name = "launch_year")
    private Integer launchYear;
    @ForeignKey(type = ShipClass.class)
    @Column(name = "class_id")
    private ShipClass shipClass;
}
