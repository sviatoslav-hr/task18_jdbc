package com.khrystyna.models;

import com.khrystyna.annotations.Column;
import com.khrystyna.annotations.Entity;
import com.khrystyna.annotations.PrimaryKey;
import com.khrystyna.annotations.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "ship_class")
public class ShipClass {
    @PrimaryKey
    private Integer id;
    @Column(name = "name", length = 50)
    private String name;
    @Column(name = "type", length = 2)
    private String type;
    @Column(name = "country", length = 20)
    private String country;
    @Column(name = "guns_num")
    private Integer gunsNumber;
    private Float bore;
    private Integer displacement;
}
