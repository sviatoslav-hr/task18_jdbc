package com.khrystyna.reflection;

import com.khrystyna.annotations.Column;
import com.khrystyna.annotations.Entity;
import com.khrystyna.annotations.Table;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private Class<T> clazz;
    // if @Table.name = "" set name as class.name.toLowerCase()
    // if field does not contains @Column annotation, set field.name as name


    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public String getTableName() {
        if (clazz.isAnnotationPresent(Table.class)) {
            Table[] annotations = clazz.getAnnotationsByType(Table.class);
            return annotations[0].name();
        } else {
            return clazz.getSimpleName().toLowerCase();
        }
    }

    public T transform(ResultSet resultSet) {
        if (!clazz.isAnnotationPresent(Entity.class)) {
            return null;
        }
        T object = getNewInstance();
        if (object == null) {
            return null;
        }
        for (Field field : clazz.getDeclaredFields()) {
            Object value = getField(field, resultSet);
            try {
                field.set(object, value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return object;
    }

    private Object getField(Field field, ResultSet resultSet) {
        String columnName;
        field.setAccessible(true);
        if (field.isAnnotationPresent(Column.class)) {
            Column[] annotations = field.getAnnotationsByType(Column.class);
            columnName = annotations[0].name();
        } else {
            columnName = field.getName().toLowerCase();
        }
        try {
            return resultSet.getObject(columnName);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Constructor<T> getDefaultConstructor() {
        try {
            return clazz.getConstructor();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

    private T getNewInstance() {
        Constructor<T> constructor = getDefaultConstructor();
        if (constructor == null) {
            return null;
        }
        try {
            return constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
