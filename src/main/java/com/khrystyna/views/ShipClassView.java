package com.khrystyna.views;

import com.khrystyna.dao.ShipClassDao;
import com.khrystyna.dao.impl.ShipClassDaoImpl;
import com.khrystyna.models.ShipClass;

import java.sql.SQLException;

public class ShipClassView implements View {
    public void print() {
        TablePrinter printer = new TablePrinter("id", "name", "type", "country",
                "guns_num", "bore", "displacement");
        ShipClassDao shipDao = new ShipClassDaoImpl();
        try {
            for (ShipClass shipClass : shipDao.findAll()) {
                printer.addRow(shipClass.getId(), shipClass.getName(), shipClass.getType(),
                        shipClass.getCountry(), shipClass.getGunsNumber(),
                        shipClass.getBore(), shipClass.getDisplacement());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        printer.print();
    }
}
