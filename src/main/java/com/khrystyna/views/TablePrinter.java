package com.khrystyna.views;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TablePrinter {
    private List<Row> rows = new LinkedList<>();
    private List<Column> columns = new LinkedList<>();

    public void addRow(Object... cols) {
        List<String> strings = Arrays.stream(cols)
                .map(Object::toString)
                .collect(Collectors.toList());
        rows.add(new Row(
                strings.toArray(new String[columns.size()])
        ));
        updateColumns(strings);
    }

    private void updateColumns(List<String> strings) {
        columns.forEach(column -> column.compareLength(strings.remove(0)));
    }

    public TablePrinter(String... cols) {
        Arrays.asList(cols)
                .forEach(s -> columns.add(new Column(s)));
    }

    public void print() {
        printHeader();
        for (Row row : rows) {
            for (int i = 0; i < columns.size(); i++) {
                String string = row.strings[i];
                String alignDirection = isNumeric(string) ? "" : "-";
                Column column = columns.get(i);
                if (i == columns.size() - 1) {
                    System.out.printf("%" + alignDirection + column.length + "s%n", string);
                } else {
                    System.out.printf("%" + alignDirection + column.length + "s | ", string);
                }
            }
        }
    }

    private static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void printHeader() {
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if (i == columns.size() - 1) {
                System.out.printf("%" + column.length + "s%n", column.name);
            } else {
                System.out.printf("%" + column.length + "s | ", column.name);
            }
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < columns.size(); i++) {
            int length = columns.get(i).length;
            if (i != 0 && i != columns.size() - 1) {
                length++;
            }
            for (int j = 0; j <= length; j++) {
                builder.append('-');
            }
            if (i == columns.size() - 1) {
                builder.append('-');
            } else {
                builder.append('+');
            }
        }
        System.out.println(builder.toString());
    }

    private class Row {
        String[] strings;

        Row(String[] strings) {
            this.strings = strings;
        }
    }

    private class Column {
        String name;
        int length;

        Column(String name) {
            this.name = name;
            this.length = name.length();
        }

        void compareLength(String col) {
            int length = col.length();
            if (length > this.length) {
                this.length = length;
            }
        }
    }
}
