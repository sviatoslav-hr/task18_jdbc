package com.khrystyna.views;

@FunctionalInterface
public interface View {
    void print();
}
